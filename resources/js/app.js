import { createApp } from 'vue';
import Away from './Components/Away.vue';
import Rise from './Components/Rise.vue';
import Towards from './Components/Towards.vue';

const components = {
    Away,
    Rise,
    Towards
};

createApp({ components }).mount('#app');
