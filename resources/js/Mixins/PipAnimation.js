import CanvasAnimation from '../Mixins/CanvasAnimation';
import MousePosition from '../Mixins/MousePosition';

export default {
    mixins: [CanvasAnimation, MousePosition],

    mounted() {
        this.target.x = (this.width() / 2);
        this.target.y = (this.height() / 2);

        while (this.pips.length < this.countPips) {
            this.pips.push(this.createPip());
        }
    },

    data() {
        return {
            pips: [],
            target: {
                x: 0,
                y: 0
            }
        };
    },

    props: {
        countPips: {
            type: Number,
            default: 100
        },

        maxLifespan: {
            type: Number,
            default: 10000
        },

        maxSize: {
            type: Number,
            default: 8
        },

        maxSpeed: {
            type: Number,
            default: 8
        },

        minLifespan: {
            type: Number,
            default: 500
        },

        minSize: {
            type: Number,
            default: 2
        },

        minSpeed: {
            type: Number,
            default: 1
        }
    },

    methods: {
        update(delta) {
            //
        },

        render(delta) {
            this.context.globalAlpha = 0.5;
            this.context.clearRect(this.left(), this.top(), this.width(), this.height());

            for (const [index, pip] of this.pips.entries()) {
                this.context.fillStyle = pip.color;
                this.context.beginPath();
                this.context.arc(pip.x, pip.y, pip.size / 2, 0, Math.PI * 2);
                this.context.fill();
            }
        },

        createPip(x = null, y = null, size = null, speed = null, lifespan = null, color = 'white') {
            if (x === null) x = Math.floor(Math.random() * (this.right() - this.left())) + this.left();
            if (y === null) y = Math.floor(Math.random() * (this.bottom() - this.top())) + this.top();
            if (size === null) size = Math.floor(Math.random() * (this.maxSize - this.minSize)) + this.minSize;
            if (speed === null) speed = Math.floor(Math.random() * (this.maxSpeed - this.minSpeed)) + this.minSpeed;
            if (lifespan === null) lifespan = Math.floor(Math.random() * (this.maxLifespan - this.minLifespan)) + this.minLifespan;

            const aliveSince = Date.now();
            const originalX = x;
            const originalY = y;

            return { x, y, originalX, originalY, size, speed, lifespan, aliveSince, color };
        },

        renewPip(pip) {
            const now = Date.now();
            const lifespan = now - pip.aliveSince;

            if (lifespan > pip.lifespan) {
                pip.x = pip.originalX;
                pip.y = pip.originalY;
                pip.aliveSince = now;
            }
        },

        afterMouseleave() {
            this.target.x = (this.width() / 2);
            this.target.y = (this.height() / 2);
        },

        afterMousemove(e) {
            this.target.x = this.mouse.x;
            this.target.y = this.mouse.y;
        }
    }
};
