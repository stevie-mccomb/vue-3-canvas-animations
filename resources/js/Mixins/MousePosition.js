export default {
    mounted() {
        this.$el.addEventListener('mouseenter', this.onMouseenterBound);
        this.$el.addEventListener('mouseleave', this.onMouseleaveBound);
        this.$el.addEventListener('mousemove', this.onMousemoveBound);
    },

    data() {
        return {
            defaultMouse: {
                x: 0,
                y: 0
            },

            mouse: {
                x: 0,
                y: 0
            },

            onMouseenterBound: this.onMouseenter.bind(this),
            onMouseleaveBound: this.onMouseleave.bind(this),
            onMousemoveBound: this.onMousemove.bind(this)
        };
    },

    methods: {
        beforeMouseenter() {},

        onMouseenter(e) {
            this.beforeMouseenter();
            //
            this.afterMouseenter();
        },

        afterMouseenter() {},

        beforeMouseleave() {},

        onMouseleave(e) {
            this.beforeMouseleave();
            this.mouse.x = this.defaultMouse.x;
            this.mouse.y = this.defaultMouse.y;
            this.afterMouseleave();
        },

        afterMouseleave() {},

        beforeMousemove() {},

        onMousemove(e) {
            this.beforeMousemove();
            this.mouse.x = e.offsetX;
            this.mouse.y = e.offsetY;
            this.afterMousemove();
        },

        afterMousemove() {}
    }
};
