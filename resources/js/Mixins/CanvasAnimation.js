export default {
    mounted() {
        this.canvas = this.$el;
        this.context = this.canvas.getContext('2d');

        this.$forceUpdate();

        this.$nextTick(() => {
            this.animationFrame = requestAnimationFrame(this.loopBound);
        });
    },

    data() {
        return {
            animationFrame: 0,
            canvas: null,
            context: null,
            lastUpdated: 0,
            loopBound: this.loop.bind(this),
            paused: false
        };
    },

    methods: {
        loop(timestamp) {
            const delta = (timestamp - this.lastUpdated) / 1000;

            if (!this.paused) {
                this.update(delta);
                this.render(delta);
            }

            this.lastUpdated = timestamp;

            this.animationFrame = requestAnimationFrame(this.loopBound);
        },

        update(delta) {
            //
        },

        render(delta) {
            //
        },

        width() {
            return this.$el ? this.$el.offsetWidth : 0;
        },

        height() {
            return this.$el ? this.$el.offsetHeight : 0;
        },

        top() {
            return 0;
        },

        left() {
            return 0;
        },

        bottom() {
            return this.height();
        },

        right() {
            return this.width();
        }
    }
};
